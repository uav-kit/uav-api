from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class PositionGPS(BaseModel):
    latitude: int
    longtitud: int
    altitude: int

class Attitude(BaseModel):
    pitch: int
    roll: int
    yaw: int

positiones = {}

attitudes = {}

@app.get("/position-gps/")
async def get_position_gps():
    return {"latitude": positiones.get('latitude'), "longtitud": positiones.get('longtitud'), "altitude": positiones.get('altitude')}

@app.post("/position-gps/")
async def insert_position_gps(position: PositionGPS):
    positiones['latitude'] = position.latitude
    positiones['longtitud'] = position.longtitud
    positiones['altitude'] = position.altitude
    return {"message": "Position GPS added successfully"}

@app.put("/position-gps/")
async def update_position_gps(position: PositionGPS):
    if 'latitude' not in positiones:
        return {"error": "latitude not found"}
    if 'longtitud' not in positiones:
        return {"error": "longtitud not found"}
    if 'altitude' not in positiones:
        return {"error": "altitude not found"}
    positiones['latitude'] = position.latitude
    positiones['longtitud'] = position.longtitud
    positiones['altitude'] = position.altitude
    return {"message": "Position GPS updated successfully"}


@app.get("/attitude/")
async def get_attitude():
    return {"pitch": attitudes.get('pitch'), "roll": attitudes.get('roll'), "yaw": attitudes.get('yaw')}

@app.post("/attitude/")
async def insert_attitude(attitude: Attitude):
    attitudes['pitch'] = attitude.pitch
    attitudes['roll'] = attitude.roll
    attitudes['yaw'] = attitude.yaw
    return {"message": "Position GPS added successfully"}

@app.put("/attitude/")
async def update_attitude(attitude: Attitude):
    if 'pitch' not in attitudes:
        return {"error": "pitch not found"}
    if 'roll' not in attitudes:
        return {"error": "roll not found"}
    if 'yaw' not in attitudes:
        return {"error": "yaw not found"}
    attitudes['pitch'] = attitude.pitch
    attitudes['roll'] = attitude.roll
    attitudes['yaw'] = attitude.yaw
    return {"message": "Attitude updated successfully"}