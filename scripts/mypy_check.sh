#!/bin/bash
set -Eeuo pipefail
cd "$(dirname "$(readlink -f "$0")")"/..

poetry run python -m mypy --namespace-packages api
# poetry run python -m mypy --namespace-packages tests
