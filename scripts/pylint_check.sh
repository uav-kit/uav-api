#!/bin/bash
set -Eeuo pipefail
cd "$(dirname "$(readlink -f "$0")")"/..

poetry run python -m pylint src tests --fail-under 9``